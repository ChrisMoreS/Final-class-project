<!DOCTYPE html>
<?php

  function limpiarDatos($data){
    $data = htmlentities($data);
    $data = htmlspecialchars($data);
    $data = htmlspecialchars_decode($data);
    $data = stripcslashes($data);
    $data = strip_tags($data);
    $data = html_entity_decode($data);
    return $data;
  };

  $servername = 'localhost';
  $user = 'root';
  $psswd = '';
  $DBName = 'basededatosdepartamento';


  try {
    $conn = new PDO('mysql:host='.$servername.';dbname='.$DBName.'', $user, $psswd);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo "hola $e";
  }

?>
<html lang='es' dir='ltr'>
  <head>
    <meta charset='utf-8'>
    <link rel='stylesheet' href='css/index.css'>
    <title>Formulario de Entrada</title>
  </head>
  <body>
    <?php
    echo "<header>
            <nav class='cabecera'>
             <div class='zona-navegable'>";
    ?>
              <h1><a id='linkHome' onclick="MostrarContenido('Inicio')">Inicio</a></h1>
              <h1><a id='linkMostrar' onclick="MostrarContenido('ListarUsuarios')">Dar de baja a Usuarios</a></h1>
              <h1><a id='linkRegister' onclick="MostrarContenido('AltaUsuarios')">Dar de alta a Usuarios</a></h1>
    <?php 
    echo "
              </div>
            </nav>
          </header>";
    echo "<main>
            <div class='contenedor' id='Inicio'>
              <img src='img/logdep.png' alt='Logo Departamento'>
              <h3>
                Bienvenido a la pagina de alta y baja de usuarios del Departamento
              </h3>
            </div>

            <div class='contenedor' id='AltaUsuarios' style='display: none'>
              <img src='img/logdep.png' alt='Logo Departamento'>
              <h3>Dar de Alta un Usuario</p>
              <form action='php/register.php' method='post'>
                <label for='Usuario'>Usuario</label>
                <input type='text' name='usuario' id='Usuario'>
                <label for='Contraseña'>Contraseña</label>
                <input type='password' name='passwd' id='Contraseña'>
                <label for='RContraseña'>Repite Contraseña</label>
                <input type='password' name='rpasswd' id='RContraseña'>
                <button type='submit'>Enviar</button>
                <button type='reset'>Borrar</button>
              </form>
            </div>

            <div class='contenedor' id='ListarUsuarios' style='display: none'>
              <img src='img/logdep.png' alt='Logo Departamento'>
              <h3>Lista de Usuarios</h3>
               <table>
                  <tr>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>Acción</th>
                  </tr>
                ";
                
                try {
                  $lista = $conn->query('SELECT * FROM usuarios');
                  $i = 0;
                  while ($fila = $lista->fetch()) {
                    echo "
                      <tr>
                        <td>".($i+1)."</td>
                        <td>".$fila['Usuario']."</td>
                        <td><a href='php/borrar.php?id=".$fila['ID']."'><button>dar de baja</button></a></td>
                        </tr>
                        ";
                    $i++;
                  }
                        
                } catch (\Throwable $th) {
                  $th;
                }
              echo "
                </table>
              </div>
          </main>
          <footer>
            <div class='ccommons'>
              <a rel='license' href='http://creativecommons.org/licenses/by/4.0/'>
                <img alt='Licencia Creative Commons' style='border-width:0' src='https://i.creativecommons.org/l/by/4.0/88x31.png' />
              </a>
              <p>
                <span xmlns:dct='http://purl.org/dc/terms/' property='dct:title'>Pagina Proyecto</span> por
                <span xmlns:cc='http://creativecommons.org/ns#' property='cc:attributionName'>Clase de 2ASIR de José María Pérez Pulido</span> <br>
                se distribuye bajo una <a rel='license' href='http://creativecommons.org/licenses/by/4.0/'>
                  Licencia Creative Commons Atribución 4.0 Internacional
                </a>
              </p>
            </div >
          </footer>";

    ?>
    <script src='js/app.js' charset='utf-8'></script>
  </body>
</html>
