<?php

  function limpiarDatos($data){
    $data = htmlentities($data);
    $data = htmlspecialchars($data);
    $data = htmlspecialchars_decode($data);
    $data = stripcslashes($data);
    $data = strip_tags($data);
    $data = html_entity_decode($data);
    return $data;
  };

  $servername = 'localhost';
  $user = 'root';
  $psswd = '';
  $DBName = 'basededatosdepartamento';

  $usuario = limpiarDatos($_POST['usuario']);
  $contrasena = limpiarDatos($_POST['passwd']);
  $rcontrasena = limpiarDatos($_POST['rpasswd']);

  try {
    $conn = new PDO('mysql:host='.$servername.';dbname='.$DBName.'', $user, $psswd);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  };



if (!$contrasena || !$rcontrasena || !$usuario) {
    echo "<p style='color: red'>Los datos se deben rellenar</p>";
} elseif ($contrasena === $rcontrasena) {
    try {
        $sql = $conn->prepare('INSERT INTO usuarios(usuario, contrasena) VALUES (:usuario, :contrasena);');
        $sql->bindValue(':usuario', $usuario);
        $sql->bindValue(':contrasena', password_hash($contrasena, PASSWORD_DEFAULT));
        $sql->execute();
        
        header('location: ../index.php');

    } catch (PDOException $e) {
    }
}

?>
