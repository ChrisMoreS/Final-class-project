<?php

  $servername = 'localhost';
  $user = 'root';
  $psswd = '';
  $DBName = 'basededatosdepartamento';

  function limpiarDatos($data){
    $data = htmlentities($data);
    $data = htmlspecialchars($data);
    $data = htmlspecialchars_decode($data);
    $data = stripcslashes($data);
    $data = strip_tags($data);
    $data = html_entity_decode($data);
    return $data;
  };

  try {
    $conn = new PDO('mysql:host=$servername;dbname=$DBName', $user, $psswd);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
  }

?>